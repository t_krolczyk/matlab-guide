function varargout = zad1GUI(varargin)
% ZAD1GUI MATLAB code for zad1GUI.fig
%      ZAD1GUI, by itself, creates a new ZAD1GUI or raises the existing
%      singleton*.
%
%      H = ZAD1GUI returns the handle to a new ZAD1GUI or the handle to
%      the existing singleton*.
%
%      ZAD1GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ZAD1GUI.M with the given input arguments.
%
%      ZAD1GUI('Property','Value',...) creates a new ZAD1GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before zad1GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to zad1GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help zad1GUI

% Last Modified by GUIDE v2.5 08-Jun-2017 23:15:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @zad1GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @zad1GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before zad1GUI is made visible.
function zad1GUI_OpeningFcn(hObject, eventdata, handles, varargin)
%data initialize
handles.IloscMiejca = 1000;
handles.WHnazwa = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
handles.WHzapasy = [100 40 200 220 30 40 70];
handles.startArtPos = [0 0 0 0 0 0 0];
handles.WHminIlosc = [10 5 10 50 20 10 40];
handles.WHmagazyn = zeros(2,handles.IloscMiejca);
handles.Wysylka = [0 0 0 0 0 0 0];
handles.dostawy = zeros(100, 4);
handles.wszystkieDostawy = 1;
handles.wszystkieZamowienia = 0;

% Choose default command line output for zad1GUI
handles.output = hObject;

%initialize warehouse
%funkcja lokuje produkty w magazynie
it = 1;
[~, artNb] = size(handles.WHnazwa);
for i = 1 : artNb
        handles.startArtPos(i) = it;
        handles.WHmagazyn(1, it:(it+handles.WHzapasy(i))-1) = i;
        handles.WHmagazyn(2, it:(it+handles.WHzapasy(i))-1) = 1;
        it = it + handles.WHzapasy(1, i);
end

zaaktualizujArtWMagazynie(hObject, handles);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes zad1GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);





function wyslijZamowienie(hObject, handles)
[~, artNb] = size(handles.WHnazwa);
for i = 1 : artNb
    [~, col] = find(handles.WHmagazyn(1,:)==i);
    
    doWysylki = handles.WHmagazyn(:, handles.startArtPos(i):handles.startArtPos(i)+handles.Wysylka(i)-1);
    handles.WHmagazyn(:, handles.startArtPos(i):handles.startArtPos(i)+handles.Wysylka(i)-1) = 0;
    [~, przesunO] = size(doWysylki); 
    
    %przesuwanie obiekt�w w magazynie
    for j = handles.startArtPos(i) : max(col) - przesunO - 1
        handles.WHmagazyn(:,j) = handles.WHmagazyn(:,j+przesunO);
    end
    %zerowanie miejsc na koncu
    handles.WHmagazyn(:,max(col)-przesunO + 1 : max(col)) = 0;
    
end

handles.Wysylka = zeros(1,artNb);
guidata(hObject, handles);
%delete



function sprawdzMiejsceWMag(handles, ind)
przedmiot = handles.WHmagazyn(:, ind);
if(przedmiot(1)==0)
    disp('miejsce puste');
else
    disp('miejsce zajete\n'); 
    przedmiot(1)
    przedmiot(2)
end

%delete




function rozlokujDostaweArt(hObject, handles, ind)
[~, col] = find(handles.WHmagazyn(1,:)==ind);
ostPart = handles.WHmagazyn(2,max(col));
%dostawa
handles.WHmagazyn(1,max(col)+1 : handles.startArtPos(ind)+handles.WHzapasy(ind) - 1) = ind;
handles.WHmagazyn(2,max(col)+1 : handles.startArtPos(ind)+handles.WHzapasy(ind) - 1) = ostPart+1;
handles.dostawy(handles.wszystkieDostawy, :) = [handles.wszystkieDostawy ind (handles.startArtPos(ind)+handles.WHzapasy(ind))-max(col)-1 ostPart+1];
handles.wszystkieDostawy = handles.wszystkieDostawy + 1;

[~, artNum] = size(handles.dostawy);
for i = 1 : artNum
    dostList(i) = strcat(num2str(handles.dostawy(i, 1)), {' | '}, num2str(handles.dostawy(i, 2)), {' | '}, num2str(handles.dostawy(i, 3)),{' | '},num2str(handles.dostawy(i, 4)));
end
set(handles.dostawaHistoria, 'String', dostList);
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = zad1GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%------------------------   obs�uga grafiki ---------------------------
function zaaktualizujArtWMagazynie(hObject, handles)
[~, artNum] = size(handles.WHnazwa);
for i = 1 : artNum
    [~, col] = find(handles.WHmagazyn(1,:)==i);
    [~, nrCol] = size(col);
    artList(i) = strcat(handles.WHnazwa(i), {' | '}, num2str(nrCol,'%04d'), {' | '}, num2str(min(col),'%04d'),'-',num2str(max(col),'%04d'));
end
set(handles.produktyMagazyn, 'String', artList);
guidata(hObject, handles);


% --- Executes on selection change in listaProduktow.
function listaProduktow_Callback(hObject, eventdata, handles)
% hObject    handle to listaProduktow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listaProduktow contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listaProduktow


% --- Executes during object creation, after setting all properties.
function listaProduktow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listaProduktow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function iloscProduktu_Callback(hObject, eventdata, handles)
% hObject    handle to iloscProduktu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of iloscProduktu as text
%        str2double(get(hObject,'String')) returns contents of iloscProduktu as a double


% --- Executes during object creation, after setting all properties.
function iloscProduktu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to iloscProduktu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
x = get(handles.iloscProduktu,'String');
x = str2num(x);
if(x==0)
    msgbox('Podaj ilo�� produktu! ','B��d!','error');
else
    allProd = get(handles.listaProduktow,'String');
    selectedIndex = get(handles.listaProduktow,'Value');
    [~, col] = find(handles.WHmagazyn(1,:)==selectedIndex);
    [~, nrCol] = size(col);
    if((nrCol - (handles.Wysylka(selectedIndex) + x)) < 0)
        msgbox('Nie ma wystarczaj�cej ilo�ci towaru w magazynie! ','B��d!','error');
    else
        handles.Wysylka(selectedIndex) = handles.Wysylka(selectedIndex) + x;
        [~, artNum] = size(handles.WHnazwa);
        for i = 1 : artNum
            koszykStan(i) = strcat(handles.WHnazwa(i), {' sztuk '}, num2str(handles.Wysylka(i)));
        end
        set(handles.koszykStan, 'String', koszykStan);
    end
    %selectedItem = allProd(selectedIndex, :);
    %handles.linia = str2num(selectedItem);
end
guidata(hObject, handles);

% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in zrealizujZamowienie.
function zrealizujZamowienie_Callback(hObject, eventdata, handles)
if(sum(handles.Wysylka)==0)
    msgbox('Kosz jest pusty! ','B��d!','error');
else % wszystko ok, obsu�ugjemy zamowienie
        [~, artNb] = size(handles.WHnazwa);
    for i = 1 : artNb
        [~, col] = find(handles.WHmagazyn(1,:)==i);

        doWysylki = handles.WHmagazyn(:, handles.startArtPos(i):handles.startArtPos(i)+handles.Wysylka(i)-1);
        handles.WHmagazyn(:, handles.startArtPos(i):handles.startArtPos(i)+handles.Wysylka(i)-1) = 0;
        [~, przesunO] = size(doWysylki); 

        %przesuwanie obiekt�w w magazynie
        for j = handles.startArtPos(i) : max(col) - przesunO - 1
            handles.WHmagazyn(:,j) = handles.WHmagazyn(:,j+przesunO);
        end
        %zerowanie miejsc na koncu
        handles.WHmagazyn(:,max(col)-przesunO + 1 : max(col)) = 0;

    end

    handles.Wysylka = zeros(1,artNb);
    
    %ustawiamy reszte zmiennych
    
    handles.wszystkieZamowienia = handles.wszystkieZamowienia + 1;
    set(handles.liczbaZamowien, 'String', num2str(handles.wszystkieZamowienia));
    zaaktualizujArtWMagazynie(hObject, handles);
    
    [~, artNum] = size(handles.WHnazwa);
        for i = 1 : artNum
            koszykStan(i) = strcat(handles.WHnazwa(i), {' sztuk '}, num2str(handles.Wysylka(i)));
        end
        set(handles.koszykStan, 'String', koszykStan);
    guidata(hObject, handles);

    %sprawdzamy czy nie  trzeba zrobi� dostawy
    for ind = 1 : artNb
        [~, col] = find(handles.WHmagazyn(1,:)==ind);
        [~, counter] = size(col);
        if(counter < handles.WHminIlosc(ind))
            [~, col] = find(handles.WHmagazyn(1,:)==ind);
            ostPart = handles.WHmagazyn(2,max(col));
            %dostawa
            handles.WHmagazyn(1,max(col)+1 : handles.startArtPos(ind)+handles.WHzapasy(ind) - 1) = ind;
            handles.WHmagazyn(2,max(col)+1 : handles.startArtPos(ind)+handles.WHzapasy(ind) - 1) = ostPart+1;
            handles.dostawy(handles.wszystkieDostawy, :) = [handles.wszystkieDostawy ind (handles.startArtPos(ind)+handles.WHzapasy(ind))-max(col)-1 ostPart+1];
            handles.wszystkieDostawy = handles.wszystkieDostawy + 1;

            [artNum, ~] = find(handles.dostawy);
            artNum = length(unique(artNum));
            for i = 1 : artNum
                dostList(i) = strcat(num2str(handles.dostawy(i, 1)), {' | '}, num2str(handles.dostawy(i, 2)), {' | '}, num2str(handles.dostawy(i, 3)),{' | '},num2str(handles.dostawy(i, 4)));
            end
            set(handles.dostawaHistoria, 'String', dostList);
            zaaktualizujArtWMagazynie(hObject, handles);
        end    
    end
end



function numerMiejscaDoSpr_Callback(hObject, eventdata, handles)
% hObject    handle to numerMiejscaDoSpr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numerMiejscaDoSpr as text
%        str2double(get(hObject,'String')) returns contents of numerMiejscaDoSpr as a double


% --- Executes during object creation, after setting all properties.
function numerMiejscaDoSpr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numerMiejscaDoSpr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in sprawdzMiejsceButton.
function sprawdzMiejsceButton_Callback(hObject, eventdata, handles)
x = get(handles.numerMiejscaDoSpr,'String');
ind = str2num(x);
if(ind < 1 || ind > handles.IloscMiejca)
    msgbox('W magazynie nie ma takiego miejsca! ','B��d!','error');
else
    przedmiot = handles.WHmagazyn(:, ind);
if(przedmiot(1,1)==0)
    set(handles.statusMiejscaTag, 'foregroundcolor', [0 1 0]);
    set(handles.statusMiejscaTag, 'String', 'WOLNE');
    set(handles.infoOMiejscu, 'String','~');
else
    set(handles.statusMiejscaTag, 'foregroundcolor', [1 0 0]);
    set(handles.statusMiejscaTag, 'String', 'ZAJ�TE');
    set(handles.infoOMiejscu, 'String', strcat({'produkt: '}, handles.WHnazwa(przedmiot(1,1)), {' partia: '}, num2str(przedmiot(2,1))));
    %przedmiot(1)
    %przedmiot(2)
end
end
guidata(hObject, handles);
% hObject    handle to sprawdzMiejsceButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
