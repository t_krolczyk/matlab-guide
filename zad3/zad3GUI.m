function varargout = zad3GUI(varargin)
% ZAD3GUI MATLAB code for zad3GUI.fig
%      ZAD3GUI, by itself, creates a new ZAD3GUI or raises the existing
%      singleton*.
%
%      H = ZAD3GUI returns the handle to a new ZAD3GUI or the handle to
%      the existing singleton*.
%
%      ZAD3GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ZAD3GUI.M with the given input arguments.
%
%      ZAD3GUI('Property','Value',...) creates a new ZAD3GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before zad3GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to zad3GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help zad3GUI

% Last Modified by GUIDE v2.5 09-Jun-2017 03:25:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @zad3GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @zad3GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before zad3GUI is made visible.
function zad3GUI_OpeningFcn(hObject, eventdata, handles, varargin)
handles.linie = [21 22 23 24 25];
handles.dlLinii = [11 12 17 21 8]*2;
handles.Vsr = 20;
handles.Ttramw = 10;
handles.iloscTypowWag = 5;
handles.uzycieEnergiiPerTyp = [2.0 4.0 3.7 4.4 2.2];
for i = 1 : 5
    handles.czasPrzejechaniaLiniiMin(i) = (handles.dlLinii(i) / handles.Vsr)*60;
    handles.potrzebneTramwaje(i) = ceil(handles.czasPrzejechaniaLiniiMin(i) / handles.Ttramw) + 3;
end

handles.sumTram = sum(handles.potrzebneTramwaje);
handles.tramTypPerc = [0.3 0.25 0.2 0.15 0.1];
for i = 1 : 5
    handles.liczbaWolnychTramwajowDanegoTypu(i) = round(handles.tramTypPerc(i)*handles.sumTram);
end
for i = 1 : 5
    for j = 1 : handles.potrzebneTramwaje(i)
        typTram = randperm(5, 1);
        while(handles.liczbaWolnychTramwajowDanegoTypu(typTram) < 1)
            typTram = randperm(5, 1);
        end
        handles.autoPrzydzialTram(i, j) = typTram;
        handles.liczbaWolnychTramwajowDanegoTypu(typTram) = handles.liczbaWolnychTramwajowDanegoTypu(typTram) - 1;
    end
end

for i = 1 : 5
    [~, col] = find(handles.autoPrzydzialTram(i, :)==1);
    [~, col2] = find(handles.autoPrzydzialTram(i, :)==2);
    [~, col3] = find(handles.autoPrzydzialTram(i, :)==3);
    [~, col4] = find(handles.autoPrzydzialTram(i, :)==4);
    [~, col5] = find(handles.autoPrzydzialTram(i, :)==5);
    com(i) = strcat({'linia '}, num2str(handles.linie(i)), {' tramwaj�w typu I: '}, num2str(length(col)), {' typu II: '}, num2str(length(col2)), {' typu III: '}, num2str(length(col3)), {' typu IV: '}, num2str(length(col4)), {' typu V: '}, num2str(length(col5)));
    
    lTramCom(i) = strcat({'linia '}, num2str(handles.linie(i)), {' - '}, num2str(handles.potrzebneTramwaje(i)));
end
set(handles.wozyLinieInfo, 'String', com);
set(handles.liczbaPotrzebnychTram, 'String', lTramCom);
% Choose default command line output for zad3GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes zad3GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = zad3GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in przydzialWagonow.
function przydzialWagonow_Callback(hObject, eventdata, handles)
allItems = get(handles.przydzialWagonow,'String');
selectedIndex = get(handles.przydzialWagonow,'Value');
if(selectedIndex==1)
    set(handles.uipanel3, 'Visible', 'off');
    for i = 1 : 5
    handles.liczbaWolnychTramwajowDanegoTypu(i) = round(handles.tramTypPerc(i)*handles.sumTram);
    end
    for i = 1 : 5
        for j = 1 : handles.potrzebneTramwaje(i)
            typTram = randperm(5, 1);
            while(handles.liczbaWolnychTramwajowDanegoTypu(typTram) < 1)
                typTram = randperm(5, 1);
            end
            handles.autoPrzydzialTram(i, j) = typTram;
            handles.liczbaWolnychTramwajowDanegoTypu(typTram) = handles.liczbaWolnychTramwajowDanegoTypu(typTram) - 1;
        end
    end
    
else
    set(handles.uipanel3, 'Visible', 'on');
end
for i = 1 : 5
    [~, col] = find(handles.autoPrzydzialTram(i, :)==1);
    [~, col2] = find(handles.autoPrzydzialTram(i, :)==2);
    [~, col3] = find(handles.autoPrzydzialTram(i, :)==3);
    [~, col4] = find(handles.autoPrzydzialTram(i, :)==4);
    [~, col5] = find(handles.autoPrzydzialTram(i, :)==5);
    com(i) = strcat({'linia '}, num2str(handles.linie(i)), {' tramwaj�w typu I: '}, num2str(length(col)), {' typu II: '}, num2str(length(col2)), {' typu III: '}, num2str(length(col3)), {' typu IV: '}, num2str(length(col4)), {' typu V: '}, num2str(length(col5)));
end
set(handles.wozyLinieInfo, 'String', com);
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function przydzialWagonow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to przydzialWagonow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
selectedIndex = get(handles.popupmenu2,'Value');
tmp = handles.potrzebneTramwaje(selectedIndex);
for i = 1 : tmp
    tmpCom(i) = strcat({''}, num2str(i));
end
set(handles.numerTram, 'String', tmpCom);
guidata(hObject, handles);
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in numerTram.
function numerTram_Callback(hObject, eventdata, handles)
% hObject    handle to numerTram (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns numerTram contents as cell array
%        contents{get(hObject,'Value')} returns selected item from numerTram


% --- Executes during object creation, after setting all properties.
function numerTram_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numerTram (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in typTramwajuPop.
function typTramwajuPop_Callback(hObject, eventdata, handles)
% hObject    handle to typTramwajuPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns typTramwajuPop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from typTramwajuPop


% --- Executes during object creation, after setting all properties.
function typTramwajuPop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to typTramwajuPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
selectedLineIndex = get(handles.popupmenu2,'Value');
selectedTramIndex = get(handles.numerTram,'Value');
selectedTypeIndex = get(handles.typTramwajuPop,'Value');

handles.autoPrzydzialTram(selectedLineIndex, selectedTramIndex) = selectedTypeIndex;

for i = 1 : 5
    [~, col] = find(handles.autoPrzydzialTram(i, :)==1);
    [~, col2] = find(handles.autoPrzydzialTram(i, :)==2);
    [~, col3] = find(handles.autoPrzydzialTram(i, :)==3);
    [~, col4] = find(handles.autoPrzydzialTram(i, :)==4);
    [~, col5] = find(handles.autoPrzydzialTram(i, :)==5);
    com(i) = strcat({'linia '}, num2str(handles.linie(i)), {' tramwaj�w typu I: '}, num2str(length(col)), {' typu II: '}, num2str(length(col2)), {' typu III: '}, num2str(length(col3)), {' typu IV: '}, num2str(length(col4)), {' typu V: '}, num2str(length(col5)));
end
set(handles.wozyLinieInfo, 'String', com);
% Update handles structure
guidata(hObject, handles);
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in policzZuzycieButton.
function policzZuzycieButton_Callback(hObject, eventdata, handles)
sumEnergii = 0;
for i = 1 : 5
    for j = 1 : handles.potrzebneTramwaje(i)
        %tu trzeba sprawdzi�
        sumEnergii = sumEnergii + ((handles.czasPrzejechaniaLiniiMin(i) / 60) * handles.uzycieEnergiiPerTyp(handles.autoPrzydzialTram(i, j)))*10;
    end
end
sumEnergii = sumEnergii / 1000;
set(handles.wynikZapotrzebEnergia, 'foregroundcolor', [1 0 0]);
set(handles.wynikZapotrzebEnergia, 'String', strcat(num2str(sumEnergii),{' MWh'}));
% hObject    handle to policzZuzycieButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
