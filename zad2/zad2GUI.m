function varargout = zad2GUI(varargin)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @zad2GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @zad2GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before zad2GUI is made visible.
function zad2GUI_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
handles.aktPrzystanek = 1;
handles.linia = 194;
handles.kierunek = 1;
handles.statusDrzwi = 1;
handles.statusPojazdu = 1;
handles.dostepneKierunki = {'Krowodrza G�rka', 'Czerwone Maki'};
handles.indPrz = 1:25;
handles.nazwPrzy = {'Krowodrza G�rka', 'Krowoderskich Zuch�w', 'Batalionu Ska�a AK', 'Makowskiego', '�obz�w PKP', 'Mazowiecka', 'Biprostal', 'Kawiory', 'Czarnowiejska', 'AGH', 'Cracovia', 'Jubilat', 'Konopnickiej', 'Centrum Kongresowe', 'Szwedzka', 'Kapelanka', 'S�omiana', 'Kobierzy�ska', 'Lipi�skiego', 'Grota-Roweckiego', 'Norymberska', 'Ruczaj', 'Kampus UJ', 'Chmieleniec', 'Czerwone Maki'};

%default values
set(handles.wszystkiePrzystanki, 'String', handles.nazwPrzy(1:10));%{'Krowodrza G�rka'; 'Krowoderskich Zuch�w'; 'Batalionu Ska�a AK'; 'Makowskiego'; '�obz�w PKP'; 'Mazowiecka'; 'Biprostal'; 'Kawiory'; 'Czarnowiejska'; 'AGH'; 'Cracovia'; 'Jubilat'; 'Konopnickiej'; 'Centrum Kongresowe'; 'Szwedzka'; 'Kapelanka'; 'S�omiana'; 'Kobierzy�ska'; 'Lipi�skiego'; 'Grota-Roweckiego'; 'Norymberska'; 'Ruczaj'; 'Kampus UJ'; 'Chmieleniec'; 'Czerwone Maki'});
% Update handles structure
guidata(hObject, handles);

inicjacjaRuchu(hObject, handles);
% UIWAIT makes zad2GUI wait for user response (see UIRESUME)
% uiwait(handles.busSimulationFig);


% --- Outputs from this function are returned to the command line.
function varargout = zad2GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;






% --- Executes during object creation, after setting all properties.
function bus_CreateFcn(hObject, eventdata, handles)

yourImage = imread('graphics/bus.jpg');
axes(hObject);
imshow(yourImage)
% hObject    handle to bus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate bus


% --- Executes during object creation, after setting all properties.
function busBack_CreateFcn(hObject, eventdata, handles)
yourImage = imread('graphics/busBack.jpg');
axes(hObject);
imshow(yourImage)
% hObject    handle to busBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate busBack


% --- Executes during object creation, after setting all properties.
function busFront_CreateFcn(hObject, eventdata, handles)
yourImage = imread('graphics/busFront.jpg');
axes(hObject);
imshow(yourImage);


function inicjacjaRuchu(hObject, handles)
%---------- tworzymy zmienne do oblicze�
handles.przystanek(25) = struct('nrPrzystanku', [], 'nazwa', []);

handles.komAlarm = {'Awaria', 'Zjazd do zajezdni', 'Wyjazd na lini�', 'autobus rezerwowy', 'Za tramwaj'};
guidata(hObject, handles);

%---------- inicjalizacja ruchu
aktualnyPrzystanekDzwiek(handles.aktPrzystanek);

function aktualnyPrzystanekDzwiek(nrPrzystanku)
[a, fs] = audioread('voices/aktPrzy.mp3');
sound(a, fs);
pause(1.45)
[a, fs] = audioread(strcat('voices/p',num2str(nrPrzystanku),'.mp3'));
sound(a, fs);


function nastPrzystanekDzwiek(nrPrzystanku)
[a, fs] = audioread('voices/nastPrzy.mp3');
sound(a, fs);
pause(1.3)
[a, fs] = audioread(strcat('voices/p',num2str(nrPrzystanku),'.mp3'));
sound(a, fs);


function ostatniPrzystanekDzwiek(nrPrzystanku)
[a, fs] = audioread('voices/petla.mp3');
sound(a, fs);
pause(1.3)
[a, fs] = audioread(strcat('voices/p',num2str(nrPrzystanku),'.mp3'));
sound(a, fs);


% --- Executes on button press in nastepnyPrzystanekButton.
function nastepnyPrzystanekButton_Callback(hObject, eventdata, handles)
if(handles.statusPojazdu==0)
    msgbox('Autobus aktualnie nie obs�uguje przystank�w.','Komuniat specjalny!','error');
elseif(handles.statusDrzwi==1)
    if(handles.kierunek==0)
        handles.aktPrzystanek = handles.aktPrzystanek-1;
    else
        handles.aktPrzystanek = handles.aktPrzystanek+1;
    end;
    
    set(handles.statusPojazduTekstTag, 'String', 'w trasie');
    if(handles.aktPrzystanek < 2)
        handles.kierunek = 1;
        set(handles.wyborKierunku, 'Value', 1);
        ostatniPrzystanekDzwiek(handles.aktPrzystanek);
    elseif(handles.aktPrzystanek > size(handles.indPrz) - 1)
        handles.kierunek = 0;
        set(handles.wyborKierunku, 'Value', 2);
        handles.aktPrzystanek=size(handles.indPrz) - 1;
    else
        aktualnyPrzystanekDzwiek(handles.aktPrzystanek);
    end
    %pause(3);
    ustawDaneNaEkranieWew(handles);
    
    set(handles.statusPojazduTekstTag, 'String', {'przystanek'; 'drzwi zamkni�te'});
    
    sortujNastPrzystanki(handles);
else
    msgbox('Zanim ruszysz dalej zamknij drzwi bo pasa�erowie wypadn� :D ','Drzwi otwarte!','error');
end
guidata(hObject, handles);

%ustawia aktualny i nastepny przystanek na ekranie wewn�trznym
function ustawDaneNaEkranieWew(handles)
    set(handles.wewEkranPrzystanek, 'String', handles.nazwPrzy(handles.aktPrzystanek));
    if(handles.kierunek==1)
        set(handles.wewEkranNastPrzystanek, 'String', handles.nazwPrzy(handles.aktPrzystanek+1));
    else
        set(handles.wewEkranNastPrzystanek, 'String', handles.nazwPrzy(handles.aktPrzystanek-1));
    end

    %uk�ada nastepne przystanki w kolejno�ci jazdy
function sortujNastPrzystanki(handles)
%nastepne przystanki 
    if(handles.kierunek==1)
        [~, max] = size(handles.indPrz);
        if((handles.aktPrzystanek+10)<=max)
            przystanki = handles.nazwPrzy(handles.aktPrzystanek:handles.aktPrzystanek+10);
        else
            przystanki = handles.nazwPrzy(handles.aktPrzystanek:max);
        end
    else
        max = 1;
        if((handles.aktPrzystanek-10) < max)
            przystanki = handles.nazwPrzy(max:handles.aktPrzystanek);
        else
            przystanki = handles.nazwPrzy(handles.aktPrzystanek-10:handles.aktPrzystanek);
        end
        przystanki = flip(przystanki);
    end

    set(handles.wszystkiePrzystanki, 'String', przystanki);

% --- Executes on selection change in specjalnyKomunikatLista.
function specjalnyKomunikatLista_Callback(hObject, eventdata, handles)
allItems = get(handles.specjalnyKomunikatLista,'string');
selectedIndex = get(handles.specjalnyKomunikatLista,'Value');
if(selectedIndex~=1)
    handles.statusPojazdu=0;
    set(handles.busBokScreen, 'String', allItems(selectedIndex, :));
    set(handles.busPrzodScreen, 'String', allItems(selectedIndex, :));
    set(handles.statusPojazduTekstTag, 'String', 'w trasie');
    set(handles.wewEkranKierunek, 'String', allItems(selectedIndex, :));
    set(handles.wewEkranPrzystanek, 'String', allItems(selectedIndex, :));
    set(handles.wewEkranNastPrzystanek, 'String', allItems(selectedIndex, :));
else
    handles.statusPojazdu=1;
    set(handles.busBokScreen, 'String', strcat(num2str(handles.linia), {' '} , handles.dostepneKierunki(handles.kierunek+1)));
    set(handles.busPrzodScreen, 'String', strcat(num2str(handles.linia), {' '} , handles.dostepneKierunki(handles.kierunek+1)));
    handles.kierunek = 1;
    handles.aktPrzystanek = 1;
    nastPrzystanekDzwiek(handles.aktPrzystanek);
    set(handles.statusPojazduTekstTag, 'String', {'przystanek'; 'drzwi zamkni�te'});
    set(handles.wewEkranKierunek, 'String', allItems(selectedIndex, :));
    %wewn�trzy ekran
    ustawDaneNaEkranieWew(handles)
    set(handles.wewEkranKierunek, 'String', handles.dostepneKierunki(handles.kierunek+1));
end
guidata(hObject, handles);




% --- Executes on selection change in wyborKierunku.
function wyborKierunku_Callback(hObject, eventdata, handles)
if(handles.aktPrzystanek==1 || handles.aktPrzystanek==25)
    msgbox('Nie mo�na jecha� w tym kierunku, osi�gni�to koniec trasy!','koniec trasy!!','error');
else
handles.kierunek = 1 - handles.kierunek;
    if(handles.statusPojazdu~=0)
        sortujNastPrzystanki(handles);
        set(handles.busBokScreen, 'String', strcat(num2str(handles.linia), {' '} , handles.dostepneKierunki(handles.kierunek+1)));
        set(handles.busPrzodScreen, 'String', strcat(num2str(handles.linia), {' '} , handles.dostepneKierunki(handles.kierunek+1)));
        set(handles.wewEkranKierunek, 'String', handles.dostepneKierunki(handles.kierunek+1));
    end
end
   
guidata(hObject, handles);
% hObject    handle to wyborKierunku (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns wyborKierunku contents as cell array
%        contents{get(hObject,'Value')} returns selected item from wyborKierunku


% --- Executes on button press in kontrolaDrzwiButton.
function kontrolaDrzwiButton_Callback(hObject, eventdata, handles)
[a, fs] = audioread('voices/drzwi.wav');
sound(a, fs);
pause(3)

if(handles.statusDrzwi==1)
    set(handles.statusDrzwiValTag, 'foregroundcolor', [1 0 0]);
    set(handles.statusDrzwiValTag, 'String', 'OTWARTE');
    set(handles.kontrolaDrzwiButton, 'String', 'zamknij drzwi');
    set(handles.statusPojazduTekstTag, 'String', {'przystanek'; 'drzwi otwarte'});
    handles.statusDrzwi=0;
else
    set(handles.statusDrzwiValTag, 'foregroundcolor', [0 1 0]);
    set(handles.statusDrzwiValTag, 'String', 'ZAMKNI�TE');
    set(handles.kontrolaDrzwiButton, 'String', 'otw�rz drzwi');
    set(handles.statusPojazduTekstTag, 'String', {'przystanek'; 'drzwi zamkni�te'});
    handles.statusDrzwi=1;
    if(handles.kierunek==1)
        nastPrzystanekDzwiek(handles.aktPrzystanek+1);
    else
        nastPrzystanekDzwiek(handles.aktPrzystanek-1);
    end
end
guidata(hObject, handles);


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
allItems = get(handles.popupmenu5,'string');
selectedIndex = get(handles.popupmenu5,'Value');
selectedItem = allItems(selectedIndex, :);
handles.linia = str2num(selectedItem);
if(handles.statusPojazdu~=0)
    set(handles.busBokScreen, 'String', strcat(num2str(handles.linia), {' '} , handles.dostepneKierunki(handles.kierunek+1)));
    set(handles.busPrzodScreen, 'String', strcat(num2str(handles.linia), {' '} , handles.dostepneKierunki(handles.kierunek+1)));
    set(handles.wewEkranNrLinii, 'String', {'linia'; strcat(num2str(handles.linia))});
end
set(handles.busBackScreenTag, 'String', num2str(handles.linia));



% --- Executes during object creation, after setting all properties.
function wszystkiePrzystanki_CreateFcn(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function axes4_CreateFcn(hObject, eventdata, handles)
yourImage = imread('graphics/reklama.jpg');
axes(hObject);
imshow(yourImage)
